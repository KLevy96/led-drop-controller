# EdN Bartender ARGB LED Controller

## Introduction

This project consists in providing a way to illuminate the bartender's drop zones.
Under each drop zone, a powerful ARGB LED will be placed in order to provide the users with an easier way to visualize the spot on which their order is ready, or where to place their glass in order to reuse them.  
The ARGB LEDs (WS2812B) should be powered with a well dimensioned 5V DC power supply, and the LEDs will be controlled using an ESP8266, exposing a REST API to control the LEDs correspondig to each spot.  
The ESP8266 is connected to a WiFi network, and exposes its routes via its IP address on port 80.

## How to use the Project

First of all, an ESP8266 should be procured, whatever the format. Then you can flash the firmware contained in *led-drop-controller.ino*, modifying the constants contained in *const.h* if necessary. This header file contains all the parameters of your setup : WiFi's credentials, pin to send the data to, amount of LEDs to control, ...  
In order to compile and upload the firmware to an ESP8266, you can use the Arduino IDE and the ESP8266 Add-on. There are many tutorials on the Internet to learn how to use this combination, but here is a random one : https://randomnerdtutorials.com/how-to-install-esp8266-board-arduino-ide/  
Then as soon as the LEDs are connected to a power supply, the ESP8266 is powered on, and the wiring is done, you should be able to send HTTP POST requests to **/blink** to blink a LED, and to **/static** to set a LED's background color.

## Routes

- **GET /** : Should return HTTP code 200 if the ESP is ready.
- **POST /blink** : Makes a LED blink with a specified color. The number of blinking cycles is defined as constant BLINK_CYCLES.
    Takes parameters :
    - id, an int representing the index of the LED you want to blink;
    - r, an int between 0 and 255 representing red intensity;
    - g, an int between 0 and 255 representing green intensity;
    - And b, an int between 0 and 255 representing blue intensity.
    Parameters should be passed as **application/x-www-form-urlencoded**, not as a JSON.
- **POST /static** : Sets a "background color" to a LED, meaning a color that stays on when the LED isn't blinking. To turn the LED off, simply set this to black !
    Takes parameters :
    - id, an int representing the index of the LED you want to blink;
    - r, an int between 0 and 255 representing red intensity;
    - g, an int between 0 and 255 representing green intensity;
    - And b, an int between 0 and 255 representing blue intensity.
    Parameters should be passed as **application/x-www-form-urlencoded**, not as a JSON.
// WiFi SSID/password
const char SSID[] = "SSID";
const char PWD[] = "password";

const unsigned short int ARGB_PIN = 2; // D4 - refers to the pinout of a ESP8266 Wemos D1 mini
const unsigned short int ARGB_NUMBER = 6; // Number of ARGB LEDs to control

const unsigned short int BLINK_CYCLES = 6; // ON/OFF cycles of a LED blink
const unsigned short int BLINK_DELAY_MS = 500 // ON/OFF cycle delay in miliseconds

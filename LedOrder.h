#ifndef LEDORDER_H
#define LEDORDER_H

/**
 * Class reprsenting a current ARGB LED configuration
 */ 
class LedOrder {
    public:
        // The remaining blink cycles to process
        unsigned short int blinkCounter;
        // The blinking red color intensity, should range from 0 to 255
        unsigned short int blinkR;
        // The blinking green color intensity, should range from 0 to 255
        unsigned short int blinkG;
        // The blinking blue color intensity, should range from 0 to 255
        unsigned short int blinkB;
        // The background red color intensity, should range from 0 to 255
        unsigned short int backR;
        // The background green color intensity, should range from 0 to 255
        unsigned short int backG;
        // The background blue color intensity, should range from 0 to 255
        unsigned short int backB;

        /**
         * Constructor. Initializes a LedOrder object with all attributes set to 0.
         * Change these values by accessing directly the public attributes.
         */
        LedOrder();
};

#endif
#include <FastLED.h>
#include <ESP8266WiFi.h>
#include "ESPAsyncWebServer.h"
#include "const.h"
#include "LedOrder.h"


// LED definition
CRGB leds[ARGB_NUMBER];
LedOrder* ledOrders[ARGB_NUMBER];

// Web server (listening on port 80)
AsyncWebServer server(80);

/**
 * Connects to the WiFi network using the credentials set in const.h
 */
void connectWifi() {
    WiFi.mode(WIFI_STA);
    WiFi.disconnect();
    delay(100);
    Serial.println("\n\nAttempting to connect to WiFi");
    WiFi.begin(SSID, PWD);
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print('.');
    }
    Serial.println("\nWiFi Connected !");
}

/**
 * Prints to the Serial connection all data pertaining to the WiFi network.
 */
void printNetworkStatus() {
    Serial.print("Local IP : ");
    Serial.println(WiFi.localIP().toString());
    Serial.print("MAC address : ");
    Serial.println(WiFi.macAddress());
    Serial.print("SSID : ");
    Serial.println(WiFi.SSID());
    Serial.print("Signal Strength : ");
    Serial.print(WiFi.RSSI());
    Serial.println(" dBm");
}

/**
 * Sets up AsyncWebServer and routes
 */
void setupHttpServer() {
    // Declaring root handler, and action to be taken when root is requested.
    // If the ESP is able to match any route, it should respond "I'm UP!" with a 200 HTTP status code
    auto root_handler = server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
        Serial.println("=> /");
        request->send(200, "text/plain", "I'm UP!\n");
    });

    // Exposes a route to blink a specified LED.
    // Takes r, g, and b as ints ranging from 0 to 255, passed as x-www-form-urlencoded http format
    server.on("/blink", HTTP_POST, [](AsyncWebServerRequest* request){
        Serial.println("=> /blink");
        if (request->hasParam("id", true) &&
            request->hasParam("r", true)  &&
            request->hasParam("g", true)  &&
            request->hasParam("b", true)) {

            Serial.print("  id : ");
            Serial.println(request->getParam("id", true)->value());
            int id = atoi((request->getParam("id", true)->value()).c_str());
            Serial.print("  r : ");
            Serial.println(request->getParam("r", true)->value());
            int r = atoi((request->getParam("r", true)->value()).c_str());
            Serial.print("  g : ");
            Serial.println(request->getParam("g", true)->value());
            int g = atoi((request->getParam("g", true)->value()).c_str());
            Serial.print("  b : ");
            Serial.println(request->getParam("b", true)->value());
            int b = atoi((request->getParam("b", true)->value()).c_str());
            
            if (id < 0 || id > ARGB_NUMBER ||
                r < 0 || r > 255 ||
                g < 0 || g > 255 ||
                b < 0 || b > 255) {
                
                request->send(400, "text/plain", "At least one parameter has an illegal value.\n");
                Serial.println("  At least one parameter has an illegal value.");
            } else {
                ledOrders[id]->blinkCounter = BLINK_CYCLES;
                ledOrders[id]->blinkR = r;
                ledOrders[id]->blinkG = g;
                ledOrders[id]->blinkB = b;
                
                request->send(200);
                Serial.println("  awesome request bro, nice params");
            }
        } else {
            request->send(400, "text/plain", "Missing at least one POST param.\n");
            Serial.println("  Missing at least one POST param.");
        }
    });

    // Exposes a route to set the background color of a specified LED.
    // Takes r, g, and b as ints ranging from 0 to 255, passed as x-www-form-urlencoded http format
    server.on("/static", HTTP_POST, [](AsyncWebServerRequest* request){
        Serial.println("=> /static");
        if (request->hasParam("id", true) &&
            request->hasParam("r", true)  &&
            request->hasParam("g", true)  &&
            request->hasParam("b", true)) {

            Serial.print("  id : ");
            Serial.println(request->getParam("id", true)->value());
            int id = atoi((request->getParam("id", true)->value()).c_str());
            Serial.print("  r : ");
            Serial.println(request->getParam("r", true)->value());
            int r = atoi((request->getParam("r", true)->value()).c_str());
            Serial.print("  g : ");
            Serial.println(request->getParam("g", true)->value());
            int g = atoi((request->getParam("g", true)->value()).c_str());
            Serial.print("  b : ");
            Serial.println(request->getParam("b", true)->value());
            int b = atoi((request->getParam("b", true)->value()).c_str());
            
            if (id < 0 || id > ARGB_NUMBER ||
                r < 0 || r > 255 ||
                g < 0 || g > 255 ||
                b < 0 || b > 255) {
                
                request->send(400, "text/plain", "At least one parameter has an illegal value.\n");
                Serial.println("  At least one parameter has an illegal value.");
            } else {
                ledOrders[id]->backR = r;
                ledOrders[id]->backG = g;
                ledOrders[id]->backB = b;
                
                request->send(200);
                Serial.println("  awesome request bro, nice params");
            }
        } else {
            request->send(400, "text/plain", "Missing at least one POST param.\n");
            Serial.println("  Missing at least one POST param.");
        }
    });

    // If request doesn't match any route, returns 404.
    server.onNotFound([](AsyncWebServerRequest *request){
        Serial.println("=> Unknown route");
        request->send(404, "text/plain", "No route matched.\n");
    });

    server.begin();
}

// Classic Arduino pragma setup function.
// Launches a Serial connection at 9600 bauds, creates necesseary LED objects, connects to WiFi and sets up REST API routes.
void setup() {
    Serial.begin(9600);

    FastLED.addLeds<NEOPIXEL, ARGB_PIN>(leds, ARGB_NUMBER);
    for (int i = 0; i < ARGB_NUMBER; i++) {
        ledOrders[i] = new LedOrder();
    }

    connectWifi();
    printNetworkStatus();
    setupHttpServer();
}

// Classic Arduino pragma loop function.
// Treats every LED in a sequential manner, then pauses between each iteration for 500ms.
void loop() {
    for (int i = 0; i < ARGB_NUMBER; i++) {

        leds[i].r = ledOrders[i]->backR;
        leds[i].g = ledOrders[i]->backG;
        leds[i].b = ledOrders[i]->backB;
        
        if (ledOrders[i]->blinkCounter > 0) {
            if (ledOrders[i]->blinkCounter % 2 == 0) {
                leds[i].r = ledOrders[i]->blinkR;
                leds[i].g = ledOrders[i]->blinkG;
                leds[i].b = ledOrders[i]->blinkB;
            }
            ledOrders[i]->blinkCounter--;
        }
    }

    FastLED.show();

    delay(BLINK_DELAY_MS);
}
